Table of Contents
=================

* [Table of Contents](#table-of-contents)
* [<strong>LATEST VERSION v0.1.3</strong>](#latest-version-v010)
* [[WIP] Container with various toolsets and my own dotfiles](#wip-container-with-various-toolsets-and-my-own-dotfiles)
* [How to run this on your machine](#how-to-run-this-on-your-machine)
* [Notes](#notes)
* [Issues](#issues)
* [Tools installed](#tools-installed)
* [Pipeline Configuration](#pipeline-configuration)
   * [Gitlab CI](#gitlab-ci)
   * [Github Actions](#github-actions)
* [Todo](#todo)
* [Repo Tree](#repo-tree)

<!-- Created by https://github.com/ekalinin/github-markdown-toc -->

# **LATEST VERSION v0.1.3**
# [WIP] Container with various toolsets and my own dotfiles
Used for testing new clusters based on Debian 12
Feel free to fork and modify as you please
# How to run this on your machine
By running this 
```shell
docker run -ti registry.gitlab.com/frankper/mcc
```
# Notes
* User created for the container has passwordless sudo
# Issues 
* Large image size (>0.5gb)

# Tools installed 

Refer to my [bootstrap-files repo](https://gitlab.com/frankper/bootstrap-files)

Apt packages installed are [here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/system_packages_to_install_debian_slim_cloud_container.txt?ref_type=heads)
Asdf repos installed are [here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_asdf_repo.sh?ref_type=heads)

# Pipeline Configuration
## Gitlab CI 
Repo contains a gitlab pipeline [file](https://gitlab.com/frankper/mcc/-/blob/main/.gitlab-ci.yml) to build and publish with every push to main
* an image with "commit sha" tag 
* an image with "latest" tag
* an image with "git tag" tag ,if pressent
## Github Actions
WIP (at the moment blocked due to image size limitations on the [free](https://github.com/features/packages#pricing) github registry)

# Todo 
* Add tests
* Clean up Krew Addons
* Add SSH,GPG,AWS,KUBECONFIG functionality
* Correct typos in README.md as I am sure there are many :) 
* Add github actions configuration
* Reduce image size if possible 
* Reduce container image build times (currently >15 minutes)

# Repo Tree
```
├── .dockerignore
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile
├── LICENSE
└── README.md
```