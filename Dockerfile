FROM debian:bookworm-slim
RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list

# update packages 
RUN  apt update -qy \
&& apt install bash curl git wget sudo locales apt-utils zsh -qy \
&&  apt upgrade -qy

# Set and generate the locale
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt -qy install tzdata
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
RUN locale-gen

# Create a user with passwordless sudo
ARG USERNAME=user
ARG USER_UID=1000
ARG USER_GID=$USER_UID
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN adduser $USERNAME sudo

# Clone bootstrap repo
WORKDIR /home/$USERNAME
USER $USERNAME
RUN git clone https://gitlab.com/frankper/bootstrap-files.git utils

# install apt packages list and cleanup 
RUN ["/bin/zsh", "-c", "./utils/scripts/bootstrap-system-debian-slim-cloud-container.sh"]

# install dotfiles and cli tools
RUN ["/bin/zsh", "-c", "./utils/scripts/install_omzsh.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_omz_plugins.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_fzf.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_asdf.sh"]
RUN ["/bin/zsh", "-c", "sudo ./utils/scripts/install_starship.sh --yes"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_tmux_samoskin.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/bootstrap-dotfiles.sh"]
# install Dev Tools 
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_asdf_repo.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_lazy_neovim.sh"]
RUN ["/bin/zsh", "-c", "sudo ./utils/scripts/install_devbox.sh -f"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_yq.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_krew.sh"]

ENTRYPOINT ["/bin/zsh"]
ENV USER=user
